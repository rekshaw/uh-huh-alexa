{
  "intents": [
    {
      "name": "AllIntent",
      "samples": [
        "I went to a party",
        "I have a cookie",
        "Let me tell you about this party",
        "Yesterday at the mall I saw these amazing shoes",
        "So today at school"
      ],
      "slots": []
    },
    {
      "name": "AMAZON.CancelIntent",
      "samples": [
        "cancel"
      ]
    },
    {
      "name": "AMAZON.HelpIntent",
      "samples": [
        "help"
      ]
    },
    {
      "name": "AMAZON.StopIntent",
      "samples": [
        "stop"
      ]
    }
  ]
}