'use strict';

var Alexa = require("alexa-sdk");

var languageStrings = {
    'en-GB': {
        'translation': {
            'DEFAULT' : '<say-as interpret-as="interjection">uh-huh</say-as>',
            'WELCOME' :  "Hello there, tell me what's on your mind. I'm a great listener.",
            'HELP' : "I love to listen! Just keep talking to me and I'll occasionally show my enthusiasm",
            'REPROMPT' : [
                "Oh, I'm sorry, was that a question? I wasn't listening.",
                "I didn't quite get that, but, <say-as interpret-as=\"interjection\">uh-huh</say-as>",
                "I'm sorry, I was day dreaming. Keep going, I'm listening!"
            ],
            'GOODBYE' : '<say-as interpret-as="interjection">good golly</say-as>, Goodbye'
        }
    },

    'en-US': {
        'translation': {
            'DEFAULT' : '<say-as interpret-as="interjection">uh-huh</say-as>',
            'WELCOME' :  "Hello there, tell me what's on your mind. I'm a great listener.",
            'HELP' : "I love to listen! Just keep talking to me and I'll occasionally show my enthusiasm",
            'REPROMPT' : [
                "Oh, I'm sorry, was that a question? I wasn't listening.",
                "I didn't quite get that, but, <say-as interpret-as=\"interjection\">uh-huh</say-as>",
                "I'm sorry, I was day dreaming. Keep going, I'm listening!"
            ],
            'GOODBYE' : '<say-as interpret-as="interjection">aw man!</say-as>, Goodbye'
        }
    },

    'de-DE': {
        'translation': {
            'DEFAULT' : '<say-as interpret-as="interjection">uh-huh</say-as>',
            'WELCOME' :  "Hello there, tell me what's on your mind. I'm a great listener.",
            'HELP' : "I love to listen! Just keep talking to me and I'll occasionally show my enthusiasm",
            'REPROMPT' : [
                "Oh, I'm sorry, was that a question? I wasn't listening.",
                "I didn't quite get that, but, <say-as interpret-as=\"interjection\">uh-huh</say-as>",
                "I'm sorry, I was day dreaming. Keep going, I'm listening!"
            ],
            'GOODBYE' : '<say-as interpret-as="interjection">aw man!</say-as>, Goodbye'
        }
    }
};

var handlers = {
    'NewSession': function () {
        this.emit(':ask', this.t('WELCOME'), this.t('HELP') );
    },
    'allIntent': function () {
        this.emit(':ask', this.t('DEFAULT'), random(this.t('REPROMPT')) );
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', this.t('HELP') );
    },
    'AMAZON.CancelIntent': function () {
        this.emit('QuitIntent');
    },
    'AMAZON.StopIntent': function () {
        this.emit('QuitIntent');
    },
    'SessionEndedRequest': function () {
        this.emit('QuitIntent');
    },
    'QuitIntent': function () {
        this.emit(':tell', this.t('GOODBYE'));
    },
    'Unhandled': function () {
        this.emit("allIntent");
    }
};

function random(responseArray) {
    return responseArray[Math.floor(Math.random() * responseArray.length)];
}

exports.handler = function(event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.appId = 'amzn1.ask.skill.66b38e8f-f30c-4961-a443-7cb7b417d66a';
    alexa.resources = languageStrings;
    alexa.registerHandlers(handlers);
    alexa.execute();
};